
//import thư viện express
const express = require("express");

//inport thư viện path để link trang
const path = require('path');

//khởi tạo express app
const app = express();

//khai báo cổng chạy
const port = 8000;

app.use(express.static(__dirname + "/views"));

app.get("/", (req, res) => {
    console.log(__dirname);
    res.sendFile(path.join(__dirname + '/views/css'));
    res.sendFile(path.join(__dirname + '/views/images'));
    res.sendFile(path.join(__dirname + '/views/pizza365index.html'));
})

//chạy app
app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})